import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './components/Header';
import Task from './components/Task';
import { useEffect, useState } from 'react';

function App() {
  const [tasks, setTasks] = useState([26289591, 26289621]);

  useEffect(() => {
    const getTasks = async() => {
      const tasksOp = await fetchTasks();
      setTasks(tasksOp);
    }

    getTasks();
  }, [tasks])

  const fetchTasks = async() => {
    const res = await fetch('https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty');
    const data = await res.json();
    return data;
  }

  return (
    <div className="App">
      <Header title='Top Stories'></Header>
      <Task tasks={tasks}></Task>
    </div>
  );
}

export default App;
