import news from "../news.png";

const Card = ({card}) => {
    const items = card.map((item) => {
        var date = new Date(item.time).toDateString();
        var author = item.by.toUpperCase();
        return (
            <div className='card col-3 story'>
                <img alt='News Story' src={news}></img>
                <h3>{item.title}</h3>
                <div className='row'>
                    <div className='col'>{date}</div>
                    <div className='col'>{item.score} | {item.descendants}</div>
                </div>
                <hr></hr>
                <div className='row'>
                    <div className='col'><p>Story By : {author}</p></div>
                    <div className='col'><a href={item.url}>Read Complete Story</a></div>
                </div>
            </div>
        )
    })

    return (
        <div className='row'>
            {items}
        </div>
    )
}

export default Card
