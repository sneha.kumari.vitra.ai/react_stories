import Card from "./Card";
import { useEffect, useState } from 'react';

const Task = ({tasks}) => {
    const [details, setDetails] = useState([{
        "by" : "thomas",
        "descendants" : 19,
        "id" : 26289591,
        "kids" : [ 26290316, 26289886, 26290729 ],
        "score" : 123,
        "time" : 1614472678,
        "title" : "New Bone Cell Type Identified",
        "type" : "story",
        "url" : "https://www.genengnews.com/news/new-bone-cell-type-identified-could-point-to-drug-targets-for-osteoporosis-and-other-skeletal-diseases/"
    },
    {
        "by" : "thomas",
        "descendants" : 19,
        "id" : 26289591,
        "kids" : [ 26290316, 26289886, 26290729 ],
        "score" : 123,
        "time" : 1614472678,
        "title" : "New Bone Cell Type Identified",
        "type" : "story",
        "url" : "https://www.genengnews.com/news/new-bone-cell-type-identified-could-point-to-drug-targets-for-osteoporosis-and-other-skeletal-diseases/"
    }]);

    useEffect(() => {
        let isMounted = true;
        const getDetails = async() => {
            tasks.map(async(ta) => {
                const detail = await fetchDetails(ta);

                if(isMounted) {
                    setDetails(...details, detail);
                }
            })
        }

        isMounted = false;
        getDetails();
        console.log(details);
    }, [tasks, details])

    const fetchDetails = async(id) => {
        var detailUrl = 'https://hacker-news.firebaseio.com/v0/item/'+ id +'.json';
        const res = await fetch(detailUrl);
        const data = await res.json();
        return data;
    }

    return (
        <div className='container'>
            <Card card={details}></Card>
        </div>
    )
}

export default Task